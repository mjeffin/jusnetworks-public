#!/bin/bash
#
# Copyright (C) 2015 GNS3 Technologies Inc.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#
# Install GNS3 on a remote Ubuntu LTS server
# This create a dedicated user and setup all the package
# and optionnaly a VPN
#


# Read the options
USE_VPN=1
USE_IOU=1
I386_REPO=1
UNSTABLE=0
HOSTNAME=$1
echo $HOSTNAME
# extract options and their arguments into variables.
# while true ; do
#     case "$1" in
#         --with-openvpn)
#           USE_VPN=1
#           shift
#           ;;
#         --with-iou)
#           USE_IOU=1
#           shift
#           ;;
#         --with-i386-repository)
#           I386_REPO=1
#           shift
#           ;;
#         --unstable)
#           UNSTABLE=1
#           shift
#           ;;
#         --) shift ; break ;;
#         *) echo "Internal error! $1" ; exit 1 ;;
#     esac
# done
